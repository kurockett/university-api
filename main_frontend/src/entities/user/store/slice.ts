import { createSlice } from '@reduxjs/toolkit'
import { type IUser } from '../model'
import { type RootState } from '~/app/store'

interface IUserState {
  users: IUser[]
  user: IUser | null
}

const initialState: IUserState = {
  users: [],
  user: null
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {}
})

export const getUser = (state: RootState): typeof initialState['user'] => state.user.user

export const {} = userSlice.actions
