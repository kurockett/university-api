package com.example.springtest.mapper;

import com.example.springtest.dto.group.GroupDTO;
import com.example.springtest.dto.group.GroupWithStudentsDTO;
import com.example.springtest.model.Group;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface GroupDTOMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GroupDTO mapToGroupDto(Group group);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GroupWithStudentsDTO mapToGroupWithStudentsDto(Group group);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Group mapToGroup(Group GroupDTO);
}