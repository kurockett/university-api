package com.example.springtest.controller;

import com.example.springtest.dto.FacultyDTO;
import com.example.springtest.service.FacultyService;
import com.example.springtest.model.Faculty;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/faculties")
@RequiredArgsConstructor
public class FacultyController {
    private final FacultyService facultyService;

    @GetMapping
    public ResponseEntity<List<FacultyDTO>> getFaculties() {
        return ResponseEntity.ok(facultyService.getFaculties());
    }

    @GetMapping("{id}")
    public ResponseEntity<Faculty> getFacultyById(@PathVariable String id) {
        return ResponseEntity.ok(facultyService.getFacultyById(Integer.parseInt(id)));
    }

    @PostMapping
    public ResponseEntity<Faculty> addFaculty(@RequestBody Faculty faculty) {
        return ResponseEntity.ok(facultyService.addFaculty(faculty));
    }

    @GetMapping("search")
    public ResponseEntity<List<FacultyDTO>> search(
            @RequestParam(name = "keyword", required = false) String keyword
    ) {
        return ResponseEntity.ok(facultyService.searchFacultiesByKeyword(keyword.toLowerCase()));
    }
}
