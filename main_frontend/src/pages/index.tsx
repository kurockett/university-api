import {lazy} from 'react'
import {Route, Routes} from 'react-router-dom'

const ErrorPage = lazy(async () => await import('./error/page'))
const FacultyPage = lazy(async () => await import('./faculty/page'))
const SpecialityPage = lazy(async () => await import('./speciality/page'))

export const Routing = () => {
    return (
        <Routes>
            <Route path='/speciality' element={<SpecialityPage/>}/>
            <Route index element={<FacultyPage/>}/>
            <Route path='*' element={<ErrorPage/>}/>
        </Routes>
    )
}
