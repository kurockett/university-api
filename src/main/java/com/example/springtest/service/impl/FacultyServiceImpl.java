package com.example.springtest.service.impl;

import com.example.springtest.dto.FacultyDTO;
import com.example.springtest.mapper.FacultyDTOMapper;
import com.example.springtest.model.Faculty;
import com.example.springtest.repository.FacultyRepository;
import com.example.springtest.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FacultyServiceImpl implements FacultyService {
    private final FacultyRepository facultyRepository;
    private final FacultyDTOMapper facultyDTOMapper;

    @Override
    public List<FacultyDTO> getFaculties() {
        return facultyRepository
                .findAll()
                .stream()
                .map(facultyDTOMapper::mapToFacultyDto)
                .collect(Collectors.toList());
    }

    @Override
    public Faculty getFacultyById(Integer id) {
        return facultyRepository.findById(id).orElse(null);
    }

    @Override
    public Faculty addFaculty(Faculty faculty) {
        var facultyBuild = Faculty
                .builder()
                .name(faculty.getName())
                .link(faculty.getLink())
                .build();
        return facultyRepository.save(facultyBuild);
    }

    @Override
    public List<FacultyDTO> searchFacultiesByKeyword(String keyword) {
        return facultyRepository
                .findByKeyword(keyword)
                .stream()
                .map(facultyDTOMapper::mapToFacultyDto)
                .collect(Collectors.toList());
    }
}
