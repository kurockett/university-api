interface Authority {
  authority: string
}

type Role = 'MODERATOR' | 'STUDENT' | 'LECTURER' | 'ADMIN'

export interface IUser {
  id: number
  firstName: string | null
  lastName: string | null
  email: string | null
  username: string | null
  phoneNumber: string | null
  age: number | null
  isOlder: boolean
  createdDate: string | null
  updatedDate: string | null
  facultyId: number | null
  groupId: number | null
  specialityId: number | null
  password: string
  roles: Role[]
  authorities: Authority[]
}
