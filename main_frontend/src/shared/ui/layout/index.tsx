import React, { type PropsWithChildren } from 'react'
import { Header } from '~/shared/ui'

const PageLayout: React.FC<PropsWithChildren<object>> = ({ children }) => {
  return (
        <>
            <Header/>
            {children}
        </>
  )
}

export default PageLayout
