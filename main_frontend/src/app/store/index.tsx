import {combineReducers, configureStore} from '@reduxjs/toolkit'
import {useDispatch, useSelector} from 'react-redux'
import type {TypedUseSelectorHook} from 'react-redux'
import {facultyApi} from '~/entities/faculty'
import {userApi, userSlice} from '~/entities/user'
import {authSlice} from "~/entities/auth/store";

export const store = configureStore({
    reducer: combineReducers({
        auth: authSlice.reducer,
        user: userSlice.reducer,
        [userApi.reducerPath]: userApi.reducer,
        [facultyApi.reducerPath]: facultyApi.reducer,
    }),
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(userApi.middleware, facultyApi.middleware)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
