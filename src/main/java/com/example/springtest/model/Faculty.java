package com.example.springtest.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "faculties")
public class Faculty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String abbreviation;
    private String link;

    @OneToMany(mappedBy = "faculty")
    private List<Group> groups;

    @OneToMany(mappedBy = "faculty")
    private List<UserModel> students;

    @OneToMany(mappedBy = "faculty")
    private List<Speciality> specialities;
}
