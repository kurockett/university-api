package com.example.springtest.repository;

import com.example.springtest.model.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Integer> {

    @Query(
            "select f from Faculty f " +
                    "where cast(f.id as string) like %:keyword% " +
                    "or lower(f.name) like %:keyword% " +
                    "or lower(f.abbreviation) like %:keyword%"
    )
    List<Faculty> findByKeyword(String keyword);
}


