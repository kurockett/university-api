import { type IUser } from '~/entities/user'

export type IStudent = Omit<IUser, 'password' | 'authorities' | 'roles'>
