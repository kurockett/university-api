package com.example.springtest.service.impl;

import com.example.springtest.dto.user.UserDTO;
import com.example.springtest.dto.user.UserWithCredentialsDTO;
import com.example.springtest.mapper.UserDTOMapper;
import com.example.springtest.model.Role;
import com.example.springtest.model.UserModel;
import com.example.springtest.repository.FacultyRepository;
import com.example.springtest.repository.GroupRepository;
import com.example.springtest.repository.SpecialityRepository;
import com.example.springtest.repository.UserRepository;
import com.example.springtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final FacultyRepository facultyRepository;
    private final GroupRepository groupRepository;
    private final SpecialityRepository specialityRepository;
    private final UserDTOMapper userDTOMapper;

    private List<GrantedAuthority> mapRolesToAuthorities(List<Role> roles) {
        return roles
                .stream().map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> getUsers() {
        return userRepository
                .findAll()
                .stream()
                .map(userDTOMapper::mapToUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getUserById(Integer id) {
        return userDTOMapper.mapToUserDto(userRepository.findById(id).orElse(null));
    }

    @Override
    public UserDTO addUser(UserModel user) {
        var faculty = user.getFacultyId() != null ? facultyRepository.findById(user.getFacultyId()).orElse(null) : null;
        var group = user.getGroupId() != null ? groupRepository.findById(user.getGroupId()).orElse(null) : null;
        var speciality = user.getSpecialityId() != null ? specialityRepository.findById(user.getSpecialityId()).orElse(null) : null;
        if (userRepository.existsById(user.getId()) || userRepository.existsByEmail(user.getEmail())) {
            user.setFaculty(faculty);
            user.setGroup(group);
            user.setSpeciality(speciality);
            return userDTOMapper.mapToUserDto(userRepository.save(user));
        }
        var userBuild = UserModel
                .builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .age(user.getAge())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .isOlder(user.getIsOlder())
                .faculty(faculty)
                .facultyId(user.getFacultyId())
                .group(group)
                .groupId(user.getGroupId())
                .speciality(speciality)
                .specialityId(user.getSpecialityId())
                .build();
        var savedUser = userRepository.save(userBuild);
        return userDTOMapper
                .mapToUserDto(
                        userRepository
                                .findByEmail(savedUser.getEmail())
                                .orElseThrow(() -> new UsernameNotFoundException("no user..."))
                );
    }

    @Override
    public List<UserDTO> searchUserByKeyword(String keyword) {
        return userRepository
                .findByKeyword(keyword)
                .stream()
                .map(userDTOMapper::mapToUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO updateUser(UserModel user, Integer id) {
        return userDTOMapper.mapToUserDto(userRepository.save(user));
    }
}
