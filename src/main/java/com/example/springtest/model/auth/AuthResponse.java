package com.example.springtest.model.auth;

import com.example.springtest.dto.user.UserWithCredentialsDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {
    private String token;
    private UserWithCredentialsDTO user;
}
