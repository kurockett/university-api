import React, {type PropsWithChildren} from 'react'
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@mui/material'
import {type DialogProps} from '@mui/material/Dialog/Dialog'

interface ModalProps {
    open: boolean
    title: string
    contentTitle: string
    closeHandler: DialogProps['onClose']
    dialogActions?: any
}

export const ModalBase: React.FC<PropsWithChildren<ModalProps>> = ({
                                                                       title,
                                                                       open,
                                                                       contentTitle,
                                                                       closeHandler,
                                                                       dialogActions,
                                                                       children
                                                                   }) => {
    return (
        <Dialog open={open} onClose={closeHandler}>
            <DialogTitle id="alert-dialog-title">
                {title}
            </DialogTitle>
            <DialogContent>
                {contentTitle
                    ? <DialogContentText id="alert-dialog-description">
                        {contentTitle}
                    </DialogContentText>
                    : null}
                {children}
            </DialogContent>
            <DialogActions>
                {dialogActions}
            </DialogActions>
        </Dialog>
    )
}