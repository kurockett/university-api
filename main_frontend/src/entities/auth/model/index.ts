import {IUser} from "~/entities/user";

export interface IAuthRequest {
    email: string
    password: string
}
export interface IAuthResponse {
    user: IUser
    token: string
}