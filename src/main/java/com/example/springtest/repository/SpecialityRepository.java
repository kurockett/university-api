package com.example.springtest.repository;

import com.example.springtest.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {
    @Query(
            "select s from Speciality s " +
                    "where cast(s.id as string) like %:keyword% " +
                    "or lower(s.name) like %:keyword% " +
                    "or lower(s.area) like %:keyword% " +
                    "or lower(s.qualification) like %:keyword% " +
                    "or lower(s.type) like %:keyword%"
    )
    List<Speciality> findByKeyword(String keyword);
}
