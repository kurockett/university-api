package com.example.springtest.service;

import com.example.springtest.dto.FacultyDTO;
import com.example.springtest.model.Faculty;

import java.util.List;

public interface FacultyService {
    List<FacultyDTO> getFaculties();

    Faculty getFacultyById(Integer id);

    Faculty addFaculty(Faculty faculty);

    List<FacultyDTO> searchFacultiesByKeyword(String keyword);
}
