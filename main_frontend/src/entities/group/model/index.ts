export interface IGroup {
  id: number
  name: string
  facultyId: number
  specialityId: number
}
