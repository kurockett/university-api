package com.example.springtest.service;

import com.example.springtest.dto.group.GroupDTO;
import com.example.springtest.dto.group.GroupWithStudentsDTO;
import com.example.springtest.model.Group;

import java.util.List;

public interface GroupService {
    List<GroupDTO> getGroups();

    GroupWithStudentsDTO getGroupById(Integer id);

    GroupDTO addGroup(Group group);

    List<GroupDTO> searchGroupByKeyword(String keyword);
}
