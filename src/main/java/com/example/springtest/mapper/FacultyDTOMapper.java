package com.example.springtest.mapper;

import com.example.springtest.dto.FacultyDTO;
import com.example.springtest.model.Faculty;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface FacultyDTOMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    FacultyDTO mapToFacultyDto(Faculty faculty);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Faculty mapToFaculty(FacultyDTO faculty);
}