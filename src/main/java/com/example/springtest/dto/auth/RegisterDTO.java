package com.example.springtest.dto.auth;

import lombok.Data;

@Data
public class RegisterDTO {
    private String email;
    private String password;
}
