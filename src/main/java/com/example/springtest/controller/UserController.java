package com.example.springtest.controller;

import com.example.springtest.dto.user.UserDTO;
import com.example.springtest.model.UserModel;
import com.example.springtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable String id) {
        return ResponseEntity.ok(userService.getUserById(Integer.parseInt(id)));
    }

    @PostMapping
    public ResponseEntity<UserDTO> addUser(@Valid @RequestBody UserModel userModel) {
        return ResponseEntity.ok(userService.addUser(userModel));
    }

    @GetMapping("search")
    public ResponseEntity<List<UserDTO>> searchUserByKeyword(@RequestParam("keyword") String keyword) {
        return ResponseEntity.ok(userService.searchUserByKeyword(keyword));
    }

    @PutMapping("{id}")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserModel userModel, @PathVariable String id) {
        return ResponseEntity.ok(userService.updateUser(userModel, Integer.parseInt(id)));
    }
}
