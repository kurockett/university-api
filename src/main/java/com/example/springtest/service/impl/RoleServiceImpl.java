package com.example.springtest.service.impl;

import com.example.springtest.dto.RoleDTO;
import com.example.springtest.mapper.RoleDTOMapper;
import com.example.springtest.model.Role;
import com.example.springtest.repository.RoleRepository;
import com.example.springtest.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    private final RoleDTOMapper roleDTOMapper;

    @Override
    public List<RoleDTO> getRoles() {
        return roleRepository.findAll().stream().map(roleDTOMapper::mapToRoleDto).collect(Collectors.toList());
    }

    @Override
    public Role getRoleById(Integer id) {
        return roleRepository.findById(id).orElse(null);
    }

    @Override
    public RoleDTO addRole(Role role) {
        var roleBuild = Role
                .builder()
                .name(role.getName())
                .build();
        return roleDTOMapper.mapToRoleDto(roleRepository.save(roleBuild));
    }

    @Override
    public List<RoleDTO> searchRoleByKeyword(String keyword) {
        return roleRepository
                .findByKeyword(keyword)
                .stream()
                .map(roleDTOMapper::mapToRoleDto)
                .collect(Collectors.toList());
    }
}
