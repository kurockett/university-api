package com.example.springtest.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String phoneNumber;
    private Integer age;
    private Boolean isOlder;
    private Instant createdDate;
    private Instant updatedDate;
    private Integer facultyId;
    private Integer groupId;
    private Integer specialityId;
}
