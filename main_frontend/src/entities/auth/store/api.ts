import {getApiUrl} from "~/shared/utils";
import {createAsyncThunk} from "@reduxjs/toolkit";
import axios, {AxiosError} from "axios";
import {IAuthRequest, IAuthResponse} from "~/entities/auth/model";

export const login = createAsyncThunk(
    'login',
    async (form: IAuthRequest) => {
        const res = await axios.post<IAuthResponse>(getApiUrl('/auth/login'), form)
        console.log(res)
        return res.data
    }
)

export const register = createAsyncThunk(
    'register',
    async (form: IAuthRequest) => {
        const res = await axios.post<IAuthResponse>(getApiUrl('/auth/register'), form)
        console.log(res)
        return res.data
    }
)