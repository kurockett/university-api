package com.example.springtest.service;

import com.example.springtest.dto.RoleDTO;
import com.example.springtest.model.Role;

import java.util.List;

public interface RoleService {
    List<RoleDTO> getRoles();

    Role getRoleById(Integer id);

    RoleDTO addRole(Role role);

    List<RoleDTO> searchRoleByKeyword(String keyword);
}
