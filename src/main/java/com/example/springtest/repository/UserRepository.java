package com.example.springtest.repository;

import com.example.springtest.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
    @Query(
            "select u from UserModel u " +
                    "where cast(u.id as string) like %:keyword% " +
                    "or lower(u.firstName) like %:keyword% " +
                    "or lower(u.lastName) like %:keyword% " +
                    "or lower(u.phoneNumber) like %:keyword% " +
                    "or lower(u.email) like %:keyword%"
    )
    List<UserModel> findByKeyword(String keyword);

    Optional<UserModel> findByEmail(String email);

    Boolean existsByEmail(String email);
}
