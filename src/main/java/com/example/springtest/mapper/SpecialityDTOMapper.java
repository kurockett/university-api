package com.example.springtest.mapper;

import com.example.springtest.dto.SpecialityDTO;
import com.example.springtest.model.Speciality;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface SpecialityDTOMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    SpecialityDTO mapToSpecialityDto(Speciality speciality);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Speciality mapToSpeciality(SpecialityDTO speciality);

}
