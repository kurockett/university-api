package com.example.springtest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "specialities")
public class Speciality {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "speciality_name")
    private String name;

    @Column(name = "speciality_area", unique = true)
    private String area;

    private String type;
    private String qualification;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "faculty_id")
    private Faculty faculty;

    @Column(name = "faculty_id", insertable = false, updatable = false)
    private Integer facultyId;

    @OneToMany(mappedBy = "speciality")
    private List<Group> groups;

    @OneToMany(mappedBy = "speciality")
    private List<UserModel  > students;
}
