import { useMemo } from 'react'
import { getUser } from '~/entities/user'
import { useAppSelector } from '~/app/store'

export const useUserRole = () => {
  const user = useAppSelector(getUser)
  const isAdmin = useMemo<boolean>(
    () => user?.roles.includes('ADMIN') ?? false,
    []
  )
  const isStudent = useMemo<boolean>(
    () => user?.roles.includes('STUDENT') ?? false,
    []
  )
  const isModerator = useMemo<boolean>(
    () => user?.roles.includes('MODERATOR') ?? false,
    []
  )
  const isLecturer = useMemo<boolean>(
    () => user?.roles.includes('LECTURER') ?? false,
    []
  )
  return {
    isLecturer,
    isStudent,
    isModerator,
    isAdmin
  }
}
