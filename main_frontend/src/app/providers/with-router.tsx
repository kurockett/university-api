import { Suspense } from 'react'
import { BrowserRouter } from 'react-router-dom'
import LoadingPage from '~/pages/loading/page'

export const withRouter = (component: () => React.ReactNode) => () =>
  (
    <BrowserRouter>
      <Suspense fallback={<LoadingPage />}>{component()}</Suspense>
    </BrowserRouter>
  )
