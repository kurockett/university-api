import {IUser, userSlice} from "~/entities/user";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IAuthResponse} from "~/entities/auth/model";
import {login} from "./api";

interface IAuthState {
    token: string | null,
    user: IUser | null
    error: string | null
}

const initialState: IAuthState = {
    token: null,
    user: null,
    error: null
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(login.fulfilled, (state, {payload: {token, user}}) => {
            state.user = user;
            state.token = token;
        });
    }
})