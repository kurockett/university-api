import {SyntheticEvent} from "react";
import {TextField, Box, Button} from "@mui/material";
import {getFormData} from "~/shared/utils";
import {useAppDispatch} from "~/app/store";
import {login} from "~/entities/auth/store";
import {IAuthRequest} from "~/entities/auth/model";
import { useNavigate } from "react-router-dom";

export const SignInForm = () => {
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const submitForm = (event: SyntheticEvent<HTMLFormElement>) => {
        event.preventDefault()
        const form = getFormData(event.currentTarget) as unknown as IAuthRequest
        dispatch(login(form))
            .then(() => {
                navigate('/speciality')
            })
    }
    return (
        <Box component={'form'} onSubmit={submitForm}>
            <TextField
                name={'email'}
                label="Эл. почта"
                placeholder='Введите почту'
            />
            <TextField
                name={'password'}
                label='Пароль'
                placeholder='Введите пароль'
            />
            <Button type='submit'>Отправить</Button>
        </Box>
    );
};
