package com.example.springtest.mapper;

import com.example.springtest.dto.user.UserDTO;
import com.example.springtest.dto.user.UserWithCredentialsDTO;
import com.example.springtest.model.UserModel;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UserDTOMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserDTO mapToUserDto(UserModel user);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserWithCredentialsDTO mapToUserWithCredentialsDto(UserModel user);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserModel mapToUser(UserDTO user);
}
