package com.example.springtest.service.impl;

import com.example.springtest.mapper.UserDTOMapper;
import com.example.springtest.model.auth.AuthResponse;
import com.example.springtest.dto.auth.LoginDTO;
import com.example.springtest.dto.auth.RegisterDTO;
import com.example.springtest.model.UserModel;
import com.example.springtest.repository.RoleRepository;
import com.example.springtest.repository.UserRepository;
import com.example.springtest.service.AuthService;
import com.example.springtest.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDTOMapper userDTOMapper;
    private final JwtService jwtService;

    @Override
    public AuthResponse register(RegisterDTO registerDTO) {
        var roles = roleRepository.findByName("STUDENT");
        var user = UserModel
                .builder()
                .email(registerDTO.getEmail())
                .password(passwordEncoder.encode(registerDTO.getPassword()))
                .roles(Collections.singletonList(roles))
                .build();
        userRepository.save(user);
        var token = jwtService.generateToken(user);
        return AuthResponse
                .builder()
                .token(token)
                .build();
    }

    @Override
    public AuthResponse login(LoginDTO loginDTO) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getEmail(),
                        loginDTO.getPassword()
                )
        );
        var user = userRepository.findByEmail(loginDTO.getEmail())
                .orElseThrow();
        var mappedUser = userDTOMapper.mapToUserWithCredentialsDto(user);
        var token = jwtService.generateToken(user);
        return AuthResponse
                .builder()
                .user(mappedUser)
                .token(token)
                .build();
    }
}
