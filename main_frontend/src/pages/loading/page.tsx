import React from 'react'
import { Loader } from '~/shared/ui'
import Box from '@mui/material/Box'

const LoadingPage = () => {
  return (
        <Box>
            <Loader/>
        </Box>
  )
}

export default LoadingPage
