import React, {cloneElement, FC, useState} from 'react';
import {Button} from "@mui/material";
import {SignInForm} from "~/entities/auth/ui/index";
import {ModalBase} from "~/shared/ui/modal";

interface SignInModalProps {
    element: React.ReactElement
}

export const SignInModal: FC<SignInModalProps> = ({element}) => {
    const [open, setOpen] = useState<boolean>(false)
    const toggle = () => {
        setOpen(prev => !prev)
    }
    return (
        <>
            {cloneElement(element, {
                onClick: toggle
            })}
            <ModalBase open={open} title={'Kek'} contentTitle={'Kek'} closeHandler={toggle}>
                <SignInForm/>
            </ModalBase>
        </>
    );
};
