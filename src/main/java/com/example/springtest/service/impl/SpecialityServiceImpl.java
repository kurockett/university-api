package com.example.springtest.service.impl;

import com.example.springtest.dto.SpecialityDTO;
import com.example.springtest.mapper.SpecialityDTOMapper;
import com.example.springtest.model.Speciality;
import com.example.springtest.repository.FacultyRepository;
import com.example.springtest.repository.SpecialityRepository;
import com.example.springtest.service.SpecialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpecialityServiceImpl implements SpecialityService {
    private final SpecialityRepository specialityRepository;
    private final FacultyRepository facultyRepository;
    private final SpecialityDTOMapper specialityDTOMapper;

    @Override
    public List<Speciality> getSpecialities() {
        return specialityRepository.findAll();
    }

    @Override
    public Speciality getSpecialityById(Integer id) {
        return specialityRepository.findById(id).orElse(null);
    }

    @Override
    public SpecialityDTO addSpeciality(Speciality speciality) {
        var faculty = facultyRepository.findById(speciality.getFacultyId()).orElse(null);
        var specialityBuild = Speciality
                .builder()
                .name(speciality.getName())
                .area(speciality.getArea())
                .type(speciality.getType())
                .qualification(speciality.getQualification())
                .faculty(faculty)
                .facultyId(faculty != null ? faculty.getId() : null)
                .build();
        return specialityDTOMapper.mapToSpecialityDto(specialityRepository.save(specialityBuild));
    }

    @Override
    public List<SpecialityDTO> searchSpecialityByKeyword(String keyword) {
        return specialityRepository
                .findByKeyword(keyword)
                .stream()
                .map(specialityDTOMapper::mapToSpecialityDto)
                .collect(Collectors.toList());
    }
}
