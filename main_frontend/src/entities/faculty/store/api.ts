import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { getApiUrl } from '~/shared/utils/url'
import { type IFaculty } from '../model'

export const facultyApi = createApi({
  reducerPath: 'facultyApi',
  baseQuery: fetchBaseQuery({ baseUrl: getApiUrl('/faculties') }),
  endpoints: (build) => ({
    getFaculties: build.query<IFaculty[], undefined>({
      query: () => '',
      providesTags: ['USER']
    })
  }),
  tagTypes: ['USER']
})

export const { useGetFacultiesQuery } = facultyApi
