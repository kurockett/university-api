package com.example.springtest.security;

public class SecurityConstants {
    public static String SECRET_KEY = System.getenv("SECRET_KEY");
    public final static String TOKEN_TYPE = "Bearer ";
    public final static int JWT_EXPIRATION_DATE = 1000 * 60 * 24;
}
