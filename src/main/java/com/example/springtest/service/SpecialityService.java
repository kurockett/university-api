package com.example.springtest.service;

import com.example.springtest.dto.SpecialityDTO;
import com.example.springtest.model.Speciality;

import java.util.List;

public interface SpecialityService {
    List<Speciality> getSpecialities();

    Speciality getSpecialityById(Integer id);

    SpecialityDTO addSpeciality(Speciality group);

    List<SpecialityDTO> searchSpecialityByKeyword(String keyword);
}
