package com.example.springtest.controller;

import com.example.springtest.dto.SpecialityDTO;
import com.example.springtest.model.Speciality;
import com.example.springtest.service.SpecialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/specialities")
@RequiredArgsConstructor
public class SpecialityController {
    private final SpecialityService specialityService;

    @GetMapping
    public ResponseEntity<List<Speciality>> getSpecialities() {
        return ResponseEntity.ok(specialityService.getSpecialities());
    }

    @GetMapping("{id}")
    public ResponseEntity<Speciality> getSpecialityById(@PathVariable String id) {
        return ResponseEntity.ok(specialityService.getSpecialityById(Integer.parseInt(id)));
    }

    @PostMapping
    public ResponseEntity<SpecialityDTO> addSpeciality(@RequestBody Speciality group) {
        return ResponseEntity.ok(specialityService.addSpeciality(group));
    }

    @GetMapping("search")
    public ResponseEntity<List<SpecialityDTO>> searchSpecialityByKeyword(@RequestParam("keyword") String keyword) {
        return ResponseEntity.ok(specialityService.searchSpecialityByKeyword(keyword));
    }
}
