package com.example.springtest.controller;

import com.example.springtest.dto.group.GroupDTO;
import com.example.springtest.dto.group.GroupWithStudentsDTO;
import com.example.springtest.service.GroupService;
import com.example.springtest.model.Group;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/groups")
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @GetMapping
    public ResponseEntity<List<GroupDTO>> getGroups() {
        return ResponseEntity.ok(groupService.getGroups());
    }

    @GetMapping("{id}")
    public ResponseEntity<GroupWithStudentsDTO> getGroupById(@PathVariable String id) {
        return ResponseEntity.ok(groupService.getGroupById(Integer.parseInt(id)));
    }

    @PostMapping
    public ResponseEntity<GroupDTO> addGroup(@RequestBody Group group) {
        return ResponseEntity.ok(groupService.addGroup(group));
    }

    @GetMapping("search")
    public ResponseEntity<List<GroupDTO>> searchGroupByKeyword(@RequestParam("keyword") String keyword) {
        return ResponseEntity.ok(groupService.searchGroupByKeyword(keyword));
    }
}
