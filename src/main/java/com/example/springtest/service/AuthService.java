package com.example.springtest.service;

import com.example.springtest.model.auth.AuthResponse;
import com.example.springtest.dto.auth.LoginDTO;
import com.example.springtest.dto.auth.RegisterDTO;

public interface AuthService {
    AuthResponse register(RegisterDTO registerDTO);
    AuthResponse login(LoginDTO loginDTO);
}
