package com.example.springtest.service.impl;

import com.example.springtest.dto.user.UserWithCredentialsDTO;
import com.example.springtest.mapper.UserDTOMapper;
import com.example.springtest.repository.UserRepository;
import com.example.springtest.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;

    @Override
    public UserWithCredentialsDTO getAccount(String email) {
        return userDTOMapper
                .mapToUserWithCredentialsDto(
                        userRepository
                                .findByEmail(email)
                                .orElseThrow(() -> new UsernameNotFoundException("kek"))
                );
    }
}
