package com.example.springtest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecialityDTO {
    private Integer id;
    private String name;
    private String area;
    private String type;
    private String qualification;
    private Integer facultyId;
}