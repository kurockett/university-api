package com.example.springtest.repository;

import com.example.springtest.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {
    @Query(
            "select g from Group g " +
                    "where cast(g.id as string) like %:keyword% " +
                    "or lower(g.name) like %:keyword%"
    )
    List<Group> findByKeyword(String keyword);
}
