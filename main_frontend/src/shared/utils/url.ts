export const getApiUrl = (endpoint?: string): string =>
  `${import.meta.env.VITE_BASE_API_URL}${endpoint ?? ''}`
