import { createTheme, responsiveFontSizes } from '@mui/material'
import { purple } from '@mui/material/colors'

export const theme = responsiveFontSizes(createTheme({
  palette: {
    primary: {
      main: purple['900']
    }
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true
      }
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: '#fff'
        }
      }
    },
    MuiToolbar: {
      styleOverrides: {
        root: {
          paddingLeft: 6,
          paddingRight: 6,
          '@media(min-width: 600px)': {
            minHeight: '24px',
            padding: '4px 6px'
          }
        }
      }
    },
    MuiDrawer: {
      styleOverrides: {
        paper: {
          width: 400,
          maxWidth: '100%'
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        input: {
          padding: '8px 6px'
        }
      }
    }
  }
}))
