package com.example.springtest.service;

import com.example.springtest.dto.user.UserDTO;
import com.example.springtest.model.UserModel;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService {
    List<UserDTO> getUsers();

    UserDTO getUserById(Integer id);

    UserDTO addUser(UserModel user);

    List<UserDTO> searchUserByKeyword(String keyword);

    UserDTO updateUser(UserModel user, Integer id);
}
