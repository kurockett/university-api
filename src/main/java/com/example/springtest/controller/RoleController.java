package com.example.springtest.controller;

import com.example.springtest.dto.RoleDTO;
import com.example.springtest.model.Role;
import com.example.springtest.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/roles")
@RequiredArgsConstructor
public class RoleController {
    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<List<RoleDTO>> getRoles() {
        return ResponseEntity.ok(roleService.getRoles());
    }

    @GetMapping("{id}")
    public ResponseEntity<Role> getRoleById(@PathVariable String id) {
        return ResponseEntity.ok(roleService.getRoleById(Integer.parseInt(id)));
    }

    @PostMapping
    public ResponseEntity<RoleDTO> addRole(@RequestBody Role role) {
        return ResponseEntity.ok(roleService.addRole(role));
    }

    @GetMapping("search")
    public ResponseEntity<List<RoleDTO>> search(
            @RequestParam(name = "keyword", required = false) String keyword
    ) {
        return ResponseEntity.ok(roleService.searchRoleByKeyword(keyword.toLowerCase()));
    }
}
