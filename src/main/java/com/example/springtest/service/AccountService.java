package com.example.springtest.service;

import com.example.springtest.dto.user.UserWithCredentialsDTO;

public interface AccountService {
    UserWithCredentialsDTO getAccount(String email);
}
