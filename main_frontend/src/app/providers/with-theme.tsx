import { ThemeProvider } from '@emotion/react'
import { CssBaseline } from '@mui/material'
import { theme } from '~/shared/theme'

export const withTheme = (component: () => React.ReactNode) => () =>
  (
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            {component()}
        </ThemeProvider>
  )
