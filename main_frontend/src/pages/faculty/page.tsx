import { useState } from 'react'
import { useGetFacultiesQuery } from '~/entities/faculty'
import PageLayout from '~/shared/ui/layout'
import { useUserRole } from '~/shared/hooks'
import { FacultyHeader } from '~/widgets/faculty/ui'

const Faculty = () => {
  const { data: faculties } = useGetFacultiesQuery(undefined)
  const { isAdmin } = useUserRole()
  const [open, setOpen] = useState(false)
  const toggle = () => { setOpen(prev => !prev) }
  console.log(faculties, isAdmin)
  return (
        <PageLayout>
            <FacultyHeader/>
        </PageLayout>
  )
}

export default Faculty
