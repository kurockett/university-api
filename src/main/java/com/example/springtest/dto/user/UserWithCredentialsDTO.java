package com.example.springtest.dto.user;

import com.example.springtest.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserWithCredentialsDTO extends UserDTO {
    private String password;
    private List<Role> roles;
    private Collection<? extends GrantedAuthority> authorities;
}
