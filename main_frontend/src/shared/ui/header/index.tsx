import {AccountCircle} from '@mui/icons-material'
import {
    AppBar,
    Drawer,
    IconButton,
    Toolbar,
    Typography
} from '@mui/material'
import MenuIcon from '@mui/icons-material/MenuSharp'
import React, {useState} from 'react'
import Box from '@mui/material/Box'
import {SignInModal} from "~/entities/auth/ui";

export const Header = () => {
  const [openMenu, setOpenMenu] = useState<boolean>(false)
  const toggleMenu = () => {
    setOpenMenu(prev => !prev)
  }
  return (
        <AppBar position='static'>
            <Toolbar>
                <IconButton onClick={toggleMenu}>
                    <MenuIcon
                        fontSize={'large'}
                    />
                </IconButton>
                <Drawer
                    anchor={'left'}
                    open={openMenu}
                    onClose={toggleMenu}
                    className={'header-drawer'}
                >
                    <Box>mem</Box>
                </Drawer>
                <Typography variant='h5' component='h5' sx={{flexGrow: 1, marginLeft: '1rem'}}>
                    Мой универ
                </Typography>
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    alignItems: 'center'
                }}>
                    {1 != 1 ? (
                            <IconButton>
                                <AccountCircle/>
                            </IconButton>)
                        :
                        (<SignInModal
                            element={<Typography>Войти</Typography>}
                        />)
                    }
                </Box>
            </Toolbar>
        </AppBar>
    )
}
