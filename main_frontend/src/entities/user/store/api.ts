import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query'

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: fetchBaseQuery({ baseUrl: import.meta.env.VITE_BASE_API_URL }),
  endpoints: (build) => ({}),
  tagTypes: ['USER']
})
