package com.example.springtest.dto.group;

import com.example.springtest.dto.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GroupWithStudentsDTO extends GroupDTO {
    Collection<UserDTO> students;
}
