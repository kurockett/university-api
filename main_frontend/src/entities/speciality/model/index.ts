export interface ISpeciality {
  id: number
  name: string
  area: string
  type: string
  qualification: string
  facultyId: number
}
