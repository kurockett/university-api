package com.example.springtest.dto;

import com.example.springtest.dto.group.GroupDTO;
import com.example.springtest.dto.user.UserDTO;
import com.example.springtest.dto.user.UserWithCredentialsDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FacultyDTO {
    Integer id;
    String name;
    String abbreviation;
    String link;
    Collection<GroupDTO> groups;
    Collection<UserDTO> students;
    Collection<SpecialityDTO> specialities;
}
