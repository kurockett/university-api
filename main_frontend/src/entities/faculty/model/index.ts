import { type IGroup } from '~/entities/group'
import { type ISpeciality } from '~/entities/speciality'
import { type IStudent } from '~/entities/student'

export interface IFaculty {
  id: number
  name: string
  abbreviation: string
  link: string
  groups: IGroup[]
  students: IStudent[]
  specialities: ISpeciality[]
}
