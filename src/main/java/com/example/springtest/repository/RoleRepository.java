package com.example.springtest.repository;

import com.example.springtest.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    @Query(
            "select r from Role r " +
                    "where cast(r.id as string) like %:keyword% " +
                    "or lower(r.name) like %:keyword%"
    )
    List<Role> findByKeyword(String keyword);

    Role findByName(String name);
}
