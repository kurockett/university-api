package com.example.springtest.service.impl;

import com.example.springtest.dto.group.GroupDTO;
import com.example.springtest.dto.group.GroupWithStudentsDTO;
import com.example.springtest.mapper.GroupDTOMapper;
import com.example.springtest.model.Group;
import com.example.springtest.repository.FacultyRepository;
import com.example.springtest.repository.GroupRepository;
import com.example.springtest.service.GroupService;
import com.example.springtest.repository.SpecialityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final FacultyRepository facultyRepository;
    private final SpecialityRepository specialityRepository;
    private final GroupDTOMapper groupDTOMapper;

    @Override
    public List<GroupDTO> getGroups() {
        return groupRepository.findAll().stream().map(groupDTOMapper::mapToGroupDto).collect(Collectors.toList());
    }

    @Override
    public GroupWithStudentsDTO getGroupById(Integer id) {
        return groupDTOMapper.mapToGroupWithStudentsDto(groupRepository.findById(id).orElse(null));
    }

    @Override
    public GroupDTO addGroup(Group group) {
        var faculty = facultyRepository.findById(group.getFacultyId()).orElse(null);
        var speciality = specialityRepository.findById(group.getSpecialityId()).orElse(null);
        var groupBuild = Group
                .builder()
                .name(group.getName())
                .faculty(faculty)
                .facultyId(group.getFacultyId())
                .speciality(speciality)
                .specialityId(group.getSpecialityId())
                .build();
        return groupDTOMapper.mapToGroupDto(groupRepository.save(groupBuild));
    }

    @Override
    public List<GroupDTO> searchGroupByKeyword(String keyword) {
        return groupRepository
                .findByKeyword(keyword)
                .stream()
                .map(groupDTOMapper::mapToGroupDto)
                .collect(Collectors.toList());
    }
}
